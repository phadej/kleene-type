\title{Regular expressions as types}
\author{Oleg Grenrus}
\date{2018-09-xx}

\input preamble.tex
\usepackage[euler]{cmll}

\mathchardef\mhyphen="2D

%include polycode.fmt
%format = = "\mathrel{\color{OGreen}=}"
%format -> = "\mathbin{\color{OGreen}\to}"
%format \/ = "\mathbin{\color{OGreen}\lor}"
%format <> = "\mathbin{\color{OGreen}\diamond}"
%format :: = "\mathbin{\color{OGreen}::}"
%format ++ = "\mathbin{\color{OGreen}\plus}"

%format _hash_L    = "{\color{OPurple}\#\mathit{L}}"
%format _hash_type = "{\color{OPurple}\#\mathit{type}}"
%format _hash_file = "{\color{OPurple}\#\mathit{file}}"

%format _colon_H    = "{\color{OPurple}\mathit{:\!H}}"
%format _colon_L    = "{\color{OPurple}\mathit{:\!L}}"
%format _colon_link = "{\color{OPurple}\mathit{:\!link}}"
%format _colon_path = "{\color{OPurple}\mathit{:\!path}}"
%format _colon_expr = "{\color{OPurple}\mathit{:\!expr}}"
%format _colon_type = "{\color{OPurple}\mathit{:\!type}}"
%format _colon_file = "{\color{OPurple}\mathit{:\!file}}"

%format _spec_cat  = "\mathit{s\!/\!cat}"
%format _spec_alt  = "\mathit{s\!/\!alt}"
%format _spec_opt  = "\mathit{s\!/?}"
%format _spec_star = "\mathit{s\!/\!*}"

%format string_pred = "\mathit{string?}"
%format operand_expression_pred = "\mathit{operand\mhyphen{}expression?}"

%subst string a = "\text{\color{ODarkBlue}\ttfamily \char34 " a "\char34}"

%format kleeneApply f (xs) = "\llparenthesis" "\;" f "\;" xs "\;" "\rrparenthesis\,"

\newcommand{\ctype}[1]{\ensuremath{{\color{OBlue}#1}}}
\newcommand{\typed}[2]{\ensuremath{{\color{ORed}#1} : {\color{OBlue}#2}}}

\newcommand{\typenil}[0]{\ensuremath{{\color{OGreen}\mathsf{nil}}}}
\newcommand{\typecons}[2]{\ensuremath{#1 \mathbin{\color{OGreen} \triangleleft} #2}}
\newcommand{\hlist}[1]{\ensuremath{\mathsf{HList}\;#1}}
\newcommand{\relist}[1]{\ensuremath{\mathsf{REList}\;#1}}

\newcommand{\restar}[1]{\ensuremath{#1{\color{OGreen}^\star}}}

\begin{document}
\maketitle


\begin{abstract}
Write this last.
State the problem.
Say why it’s an interesting problem.
Say what your solution achieves.
Say what follows from your solution.
\end{abstract}

%if 0
\section{Introduction}

The motivation for this work comes from \emph{Clojure spec}. Its guidelines \cite{clojure-spec} say
\blockquote{\emph{%
There is no reason to limit our specifications to what we can prove, yet that is primarily what type systems do. \ldots Thus \emph{spec} is not a type system.%
}}
In many cases dependent or quotient types let use make our types (and therefore specifications) more precise.
And there is an interesting feature in the \emph{spec}, which dependent types can model too, namely:
\blockquote{\emph{
Specs for sequences/vectors use a set of standard regular expression operators, with the standard semantics of regular expressions: %
\texttt{cat}, \texttt{alt}, \texttt{*} \ldots%
}}
Using regular expressions we can specify (tempted to say types of) functions
with a heterogeneous (but \emph{regular}) list as an argument.
For example, the interface of POSIX \texttt{find}\footnote{\url{http://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html}} utility is documented
as an (ad-hoc) regular expression
\begin{verbatim}
find [-H|-L] path... [operand_expression...]
\end{verbatim}
It's easy to imagine a Clojure library which let us execute the \texttt{find} utility as
\begin{spec}
(find _colon_L "src1" "src2" _colon_type _colon_file)
\end{spec}
And surely we can write a \emph{clojure/spec} for it.

Even \emph{spec} itself is not a type-system, we can have a type-system
with regular expressions (of types).
In this paper we'll explain how.
By stealing a little of syntax we can implement similarly ``dynamic'' |find|
and use it conveniently in GHC Haskell\footnote{GHC-8.6 to be precise}:
\begin{spec}
main = kleeneApply find (_hash_L "src1" "src2" _hash_type _hash_file)
\end{spec}
where
\begin{spec}
type FIND = (KW "H" \/ KW "L") <> S FilePath <> OPERAND_EXPRESSION
find :: REList FIND xs -> IO String
\end{spec}
\newpage
%endif

\section{Introduction}

In the very extreme, dynamic languages can have only single compound data structure: heterogeneous sequences.
Fortunately, for human operators, the sequences used as argument lists are often \emph{regular} as in \emph{regular expressions}.
For example, the interface of POSIX \texttt{find}\footnote{\url{http://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html}} utility is documented
by an (ad-hoc) regular expression
\begin{verbatim}
find [-H|-L] path... [operand_expression...]
\end{verbatim}
Given there are |String| primitive type and also primitive flag (keyword, or symbol) type family, |Flag|, we could describe \texttt{find}'s arguments' (regular) language as:
\begin{spec}
type FIND = (E \/ Flag "H" \/ Flag "L") <> S String <> OPERAND_EXPRESSION
\end{spec}
where |E| is an empty string, |\/| is an union, |<>| is a concatenation and |S| is a Kleene closure of regular expressions.

When shell scripts are run, the arguments are checked by an argument parser inside each utility.
In shell scripts arguments are also elaborated from their textual representation, so we \emph{parse} arguments, rarely we say we \emph{type-check} them.
However in many languages built-in types are more expressive, for example in \emph{Clojure},
we may call |find| procedure as:
\begin{spec}
(find _colon_L "src1" "src2" _colon_type _colon_file)
\end{spec}
where |_colon_L| is a \emph{keyword}, not a |String|.
As there are often no need to elaborate, the types of arguments are almost as often left unchecked.\footnote{bold claim, I know. They are not \emph{systematically} checked, that's my point}

Rich Hickey introduced the \emph{clojure.spec} \citep{clojure-spec} library for run-time type-checking\footnote{well, it elaborates too} of Clojure code.
One of its interesting features is the use of regular expressions to specify sequences:
\blockquote{\emph{
Specs for sequences/vectors use a set of standard regular expression operators, with the standard semantics of regular expressions: %
\texttt{cat}, \texttt{alt}, \texttt{*}, \texttt{opt} \ldots%
}}
So we can \emph{spec} |find| as
\begin{spec}
(_spec_cat  _colon_link (_spec_opt (_spec_alt _colon_H _colon_H _colon_L _colon_L))
            _colon_path (_spec_star string_pred)
            _colon_expr operand_expression_pred)
\end{spec}

Yet \emph{clojure.spec} is still only a run-time type-checking aid.
We can have a static type system with heterogeneous sequences typed
by regular expressions.
We implemented such type-system as a library for GHC Haskell.
Using our library, we can write type of |find|:
\begin{spec}
find :: REList FIND xs -> IO String
\end{spec}
using the |FIND| we already defined. Writing |REList| arguments is made
convenient by source\footnote{is there a paper?} and type-checker plugins:
\begin{spec}
main = kleeneApply find (_hash_L "src1" "src2" _hash_type _hash_file)
\end{spec}
So we get very close to the Clojure's look\&feel, in a statically typed language.

Concretely, the contributions of this paper are:
\begin{itemize}
\item A section without a name (\cref{sec:withoutaname}).
\item A design and demonstration of heterogeneous list library (\cref{sec:demo}).
\item A description of \emph{non-commutative intuitionistic linear logic with lists}.
Its fragment is isomorphic with regular expressions, an isomorphism interesting
in both directions.
\item reflections on the implementation of the GHC plugin (\cref{sec:plugin}).
\end{itemize}

We also compare our library with \citet{Gundry2015}'s library of unit-of-measure.

The \texttt{kleene-type} library is available online\footnote{where?}.


\section{Section without a name}
\label{sec:withoutaname}

First, let us start with basics.
We'll describe simply typed lambda calculus, regular expressions,
and a way to combine them.
This section is written so people without type-theory background should be able to follow.\footnote{have to explain typing rule notation}

\newpage
\subsection{Simply typed lambda calculus}

We begin by listing \emph{types} of values in a language.
The simply typed lambda calculus, STLC is the bare minimum one can have:
there are some primitive types and function types:
\begin{center}
\begin{tabular}{llcl}
    primitive types & $B$    & $::=$ & $|String| \mid \ldots$ \\
    types           & $\tau$ & $::=$ & $\tau_1 \to \tau_2 \mid B$
\end{tabular}
\end{center}
Next, one would usually define a syntax of terms,
but we'll take a longer route and talk about corresponding propositional logic first
(\citet{Wadler2015} have written an excellent overview of \emph{Propositions as types}).
In a propositional logic with only implication ($\to$) connective there are three rules:
implication introduction ($\to_I$), implication elimination ($\to_E$), and a general rule to use of hypotheses (\textsc{Hyp}):
\begin{gather*}
\prftree[r]{$\to_I$}{\Gamma, \ctype{A} \vdash \ctype{B}}{\Gamma \vdash \ctype{A \to B}} \qquad
\prftree[r]{$\to_E$}{\Gamma \vdash \ctype{A \to B}}{\Gamma \vdash \ctype{A}}{\Gamma \vdash \ctype{B}} \qquad
\prftree[r]{\sc Hyp}{\Gamma, \ctype{A} \vdash \ctype{A}}
\end{gather*}
We need to emphasize non-obvious fact that introduction and elimination rules are in a good balance.
Elimination rule is ``not too strong'' (local soundness),
in other words, by eliminating the connectiive, we don't learn anything extra, that weren't used to introduce it.
Analogously introduction rule is ``not too weak'' (local completeness),
we can always (re-)-introduce the connective from the elimination results.
\citet{Pfenning2001} explain that in great detail.

After we have a consistent logic, we can bother ourselves with the term level syntax:
\begin{gather*}
\prftree[r]{\sc App}{\Gamma \vdash \typed{f}{A \to B}}{\Gamma \vdash \typed{x}{B}}{\Gamma \vdash \typed{f\;x}{B}} \qquad
\prftree[r]{\sc Lam}{\Gamma, \typed{x}{A} \vdash \typed{e}{B}}{\Gamma \vdash \typed{\lambda x : A \to e}{A \to B}} \qquad
\prftree[r]{\sc Var}{\Gamma, \typed{x}{A} \vdash \typed{x}{A}} \qquad
\end{gather*}
The rules are a reformulation of the logic rules above, we added the \emph{proof terms}, and gave them more familiar names.
Terms are red, types are blue, refer to \citet{Wadler2015}, so it will be clear to you.

\subsection{Heterogeneous lists}

In this subsection we (try to) extend STLC with heterogeneous lists.
First we introduce new syntactic class: \emph{type-level lists}.
This is important: type-level lists are not types.
\begin{center}
\begin{tabular}{llcl}
    type list & $\Delta$ & $::=$ & $\typenil \mid \typecons{\tau}{\Delta}$ 
\end{tabular}
\end{center}
and then we extend types as with heterogeneous lists:
\begin{center}
\begin{tabular}{llcl}
    types           & $\tau$ & $::=$ & $\tau_1 \to \tau_2 \mid B \mid \mathsf{HList}\;\Delta $
\end{tabular}
\end{center}
The next step is to come up with introduction and elimination rules for $\mathsf{HList}$.
We could have
\begin{gather*}
\prftree[r]{\sc Nil}{\Gamma \vdash \ctype{\hlist{\typenil}}} \qquad
\prftree[r]{\sc Cons}{\Gamma \vdash \ctype{A}}{\Gamma \vdash \ctype{\hlist{\Delta}}}{\Gamma \vdash \ctype{\hlist{(\typecons{A}{\Delta})}}}
\end{gather*}
as introduction rules. Trying to find good elimination rules, we'll observe
that we added pairs (\ctype{A \times B}) and truth (\ctype{\top}) to our system in a very complicted way.
This doesn't work.

\subsection{Regular expressions}

Let us re-re-examine regular expressions.
The syntax of regular expressions over an alphabet $\Sigma$ is given by following grammar:
\begin{figure}[H]
\begin{center}
\begin{tabular}{llcll}
    regular expression & $r$ & $::=$  & $\varepsilon$      & empty string   \\
                       &     & $\mid$ & $a$                & $a \in \Sigma$ \\
                       &     & $\mid$ & $r_1 |<>| r_2$     & concatenation  \\
                       &     & $\mid$ & $r_1 |\/| r_2$     & alternation    \\
                       &     & $\mid$ & $\restar{r}$       & Kleene closure
\end{tabular}
\end{center}
\caption{Regular expression syntax}
\end{figure}
What strings regular expression matches is informally clear to many.
We formalise that intuition using $\Delta \Vdash r$ relation, which is read
as ``regular expression $r$ matches string $\Delta$''.
\begin{figure}[H]
\begin{gather*}
\prftree[r]{\sc Atom}{a \in \Sigma}{\typecons{a}{\typenil} \Vdash a} \qquad
\prftree[r]{\sc Eps}{\typenil \Vdash \ctype{\varepsilon}} \qquad
\prftree[r]{\sc Concat}{\Delta_1 \Vdash \ctype{r_1}}{\Delta_2 \Vdash \ctype{r_2}}{\Delta_1 |++| \Delta_2 \Vdash \ctype{r_1 |<>| r_2}} \\
\prftree[r]{\sc Left}{\Delta \Vdash \ctype{r_1}}{\Delta \Vdash \ctype{r_1 |\/| r_2}} \qquad
\prftree[r]{\sc Right}{\Delta \Vdash \ctype{r_2}}{\Delta \Vdash \ctype{r_1 |\/| r_2}} \qquad
\prftree[r]{\sc Nil}{\typenil \Vdash \ctype{r^\star}} \qquad
\prftree[r]{\sc Cons}{\Delta_1 \Vdash \ctype{r}}{\Delta_2 \Vdash \ctype{\restar{r}}}{\Delta_1 |++| \Delta_2 \Vdash \ctype{\restar{r}}}
\end{gather*}
\caption{$\Vdash$, regular expression matches relation}
\end{figure}
where |++| is string / list concatenation.

The rules above look a lot like introduction rules of some logic.
In fact, they are introduction rules of \emph{Non-Commutative Intuitionistic Linear Logic with Lists}, NCILLL, which
we describe in \cref{sec:ncilll} in more detail.
For the next subsection, we need to know that inhabitation of NCILLL is decidable\footnote{I have to give a proof :(}

\subsection{Heterogeneous lists continued}

Knowing that there is a sound logic corresponding to regular expressions, we can make another try of extending STLC with heterogeneous lists.
we also add regular expressions of types, where $\Sigma = \tau$.
With regular expressions on type level, we can extend types of STLC with \emph{regular lists}.
\begin{center}
\begin{tabular}{llcl}
    types           & $\tau$ & $::=$ & $\tau_1 \to \tau_2 \mid B \mid \mathsf{HList}\;\Delta \mid \mathsf{REList}\;r $
\end{tabular}
\end{center}

mirror right rules of NCILLL
We mirror the rules of NCILLL (\cref{fig:ncill} and \cref{fig:withlists}).
As regular expressions doesn't have \emph{implications}, $\multimap$ rules aren't needed.
For example $\otimes_R$ and $\otimes_L$ are ``lifted'' to following rules:
\begin{gather*}
\prftree[r]{\sc Concat$_I$}{\Gamma \vdash \ctype{\relist{r_1}}}{\Gamma \vdash \ctype{\relist{r_1}}}{\Gamma \vdash \ctype{\relist{(r_1 |<>| r_2)}}}
\qquad
\prftree[r]{\sc Concat$_E$}{\Gamma, \ctype{\relist{r_1}}, \ctype{\relist{r_1}} \vdash C}{\Gamma, \ctype{\relist{(r_1 |<>| r_2)}} \vdash C}
\end{gather*}
These rules looks a lot like pair (\ctype{\times}) rules.
Using regular expression concatenation, |<>|, \ctype{\mathsf{REList}} can act as a pair.
Similarly, |\/| induces sums, and $\color{OGreen}\star$ induces lists.
We could extend STLC directly with products, sums and lists, why we need regular expressions for that?

Because we can have convinience rules, making program structure very different:
\begin{gather*}
\prftree[r]{\sc RE}{\Gamma \vdash \ctype{\hlist{\Delta}}}{ \Delta \Vdash \ctype{r} }{\Gamma \vdash \ctype{\relist{r}}}
\end{gather*}
as $\Vdash$ is decidable relation, we can make machine solve it for us giving an interesting term syntax:
\begin{gather*}
\prftree[r]{\sc RE}{\Gamma \vdash \typed{xs}{\hlist{\Delta}}}{ \Delta \Vdash \ctype{r} }{\Gamma \vdash \typed{\mathsf{relist}\;xs}{\relist{r}}}
\end{gather*}
We further sugar the syntax:
\begin{center}
\begin{tabular}{llcl}
heterogeneous lists & $\llbracket x, y, z \rrbracket$            & $=$ & $\typecons{x}{\typecons{y}{\typecons{z}{\typenil}}}$ \\
regular lists       & $\llceil x, y, z \rrceil$                  & $=$ & $\mathsf{relist}\;\llbracket x, y, z \rrbracket$ \\  
regular application & $\llparenthesis f, x, y, z \rrparenthesis$ & $=$ & $f\;\llceil x, y, z \rrceil$
\end{tabular}
\end{center}

We don't implement (we could, cite antimirov)
\begin{gather*}
\prftree[r]{\sc Sub}{\Gamma \vdash \ctype{\relist{r_1}}}{\ctype{r_1} \le \ctype{r_2}}{\Gamma \vdash \ctype{\relist{r_2}}}
\end{gather*}

In conclusion, our language is as powerful as STLC with products, sums and lists; but the term structure is very different.

\section{Demonstration of the library}
\label{sec:demo}

\section{Non-Commutative Intuitionistic Linear Logic with Lists}
\label{sec:ncilll}

\begin{figure}[H]
\begin{gather*}
\prftree[r]{\sc Id}{A \Vdash A} \\
\prftree[r]{$\top_R$}{\Delta \Vdash \top} \qquad
\prftree[r]{$0_L$}{\Delta_1, 0, \Delta_2 \Vdash C} \qquad
\prftree[r]{$1_R$}{\Vdash 1} \qquad
\prftree[r]{$1_L$}{\Delta_1, \Delta_2 \Vdash C}{\Delta_1, 1, \Delta_2 \Vdash C} \\
\prftree[r]{$\varotimes_R$}{\Delta_1 \Vdash A}{\Delta_2 \Vdash B}{\Delta_1, \Delta_2 \Vdash A \varotimes B} \qquad
\prftree[r]{$\varotimes_L$}{\Delta_1, A, B, \Delta_2 \Vdash C}{\Delta_1, A \varotimes B, \Delta_2 \Vdash C} \\
\prftree[r]{$\varoplus_{R1}$}{\Delta \Vdash A}{\Delta \Vdash A \varoplus B} \qquad
\prftree[r]{$\varoplus_{R2}$}{\Delta \Vdash B}{\Delta \Vdash A \varoplus B} \qquad
\prftree[r]{$\varoplus_L$}{\Delta_1, A, \Delta_2 \Vdash C}{\Delta_1, B, \Delta_2 \Vdash C}{\Delta_1, A \varoplus B, \Delta_2 \Vdash C} \\
\prftree[r]{$\with_R$}{\Delta \Vdash A}{\Delta \Vdash B}{\Delta \Vdash A \with B} \qquad
\prftree[r]{$\with_{L1}$}{\Delta_1, A, \Delta_2 \Vdash C}{\Delta_1, A \with B, \Delta_2 \Vdash C} \qquad
\prftree[r]{$\with_{L1}$}{\Delta_1, B, \Delta_2 \Vdash C}{\Delta_1, A \with B, \Delta_2 \Vdash C} \\
\prftree[r]{$\multimap_R$}{\Delta, A \Vdash B}{\Delta \Vdash A \multimap B} \qquad
\prftree[r]{$\multimap_L$}{\Delta_1, B,\Delta_2 \Vdash C}{\Delta_1, A \multimap B, A, \Delta_2 \Vdash C} \\
\prftree[r]{$\multimapinv_R$}{A, \Delta \Vdash B}{\Delta \Vdash A \multimapinv B} \qquad
\prftree[r]{$\multimapinv_L$}{\Delta_1, B,\Delta_2 \Vdash C}{\Delta_1, A, A \multimapinv B, \Delta_2 \Vdash C}
\end{gather*}
\caption{Non-Commutative Intuitionistic Linear Logic}
\label{fig:ncill}
\end{figure}

\begin{figure}[H]
\begin{gather*}
\prftree[r]{\sc Cut}
{\Gamma \Vdash A}
{\Delta_1, A, \Delta_2 \Vdash C}
{\Delta_1, \Gamma, \Delta_2 \Vdash C}
\end{gather*}
\caption{Cut rule}
\label{fig:ncillcut}
\end{figure}

\begin{figure}[H]
\begin{gather*}
\prftree[r]{$\star_{R\mhyphen\mathsf{nil}}$}{\Vdash A^\star} \qquad
\prftree[r]{$\star_{R\mhyphen\mathsf{cons}}$}{\Delta_1 \Vdash A}{\Delta_2 \Vdash A^\star}{\Delta_1, \Delta_2 \Vdash A^\star} \qquad
\prftree[r]{$\star_{L\mhyphen\mathsf{fold}}$}
{\Delta_1, B, \Delta_2 \Vdash C}
{B \Vdash C}
{A,B \Vdash B}
{\Delta_1, A^\star, \Delta_2 \Vdash C}
\end{gather*}
\caption{\ldots with Lists}
\label{fig:withlists}
\end{figure}

\begin{figure}[H]
\begin{gather*}
TODO
\end{gather*}
\caption{\ldots with Exponentials}
\label{fig:withexps}
\end{figure}

\begin{figure}[H]
\begin{gather*}
TODO
\end{gather*}
\caption{\ldots with Commutative times}
\label{fig:withslash}
\end{figure}

\begin{gather*}
\prftree[r]{\sc Cut}
{
\prftree[r]{$\star_{R\mhyphen\mathsf{cons}}$}
{\prfsummary{\mathcal{D}_1}{\Delta_1 \Vdash A}}
{\prfsummary{\mathcal{D}_2}{\Delta_2 \Vdash A^\star}}
{\Delta_1, \Delta_2 \Vdash A^\star}
}
{
\prftree[r]{$\star_{L\mhyphen\mathsf{fold}}$}
{\prfsummary{\mathcal{E}_1}{\Psi \Vdash B}}
{\prfsummary{\mathcal{E}_2}{\Omega_1, B, \Omega_2 \Vdash C}}
{\prfsummary{\mathcal{F}}{B,A \Vdash B}}
{\Omega_1, \Psi, A^\star, \Omega_2 \Vdash C}
}
{\Omega_1, \Psi, \Delta_1, \Delta_2, \Omega_2 \Vdash C}
\\
\Downarrow
\\
\prftree[r]{\sc Cut}
{\prfsummary{\mathcal{D}_2}{\Delta_2 \Vdash A^\star}}
{
\prftree[r]{$\star_{L\mhyphen\mathsf{fold}}$}
{
\prftree[r]{\sc Cut}
{\prfsummary{\mathcal{D}_1}{\Delta_1 \Vdash A}}
{
\prftree[r]{\sc Cut}
{\prfsummary{\mathcal{E}_1}{\Psi \Vdash B}}
{\prfsummary{\mathcal{F}}{B, A \Vdash B}}
{\Psi, A \Vdash B}
}
{\Psi, \Delta_1 \Vdash B}
}
{\prfsummary{\mathcal{E}_2}{\Omega_1, B, \Omega_2 \Vdash C}}
{\prfsummary{\mathcal{F}}{B,A \Vdash B}}
{\Omega_1, \Psi, \Delta_1, A^\star, \Omega_2 \Vdash C}
}
{\Omega_1, \Psi, \Delta_1, \Delta_2, \Omega_2 \Vdash C}
\end{gather*}

\section{Reflections on GHC plugin writing}
\label{sec:plugin}

\section{TODO}

\begin{itemize}
\item ambiguity
\end{itemize}

\bibliography{bibliography}
\end{document}
